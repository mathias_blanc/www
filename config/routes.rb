Www::Application.routes.draw do
  
  ## i18n scope
  scope "(:locale)", :locale => /en|fr/ do
    
    ## static pages  
    match 'home'    => "static_pages#home",   :as => 'home',    :via => :get
    match 'admin'   => "static_pages#admin",  :as => 'admin',   :via => :get
    match 'mobile'  => "static_pages#mobile", :as => 'mobile',  :via => :get
    match 'faq'     => "static_pages#faq",    :as => 'faq',     :via => :get
    match 'contact' => 'contact#new',         :as => 'contact', :via => :get
    match 'contact' => 'contact#create',      :as => 'contact', :via => :post
    
    # REST API
    match 'all_aircrafts'      => 'aircrafts#all_aircrafts',            :as=>"all_aircrafts",       :via =>:get
    match 'all_models'         => 'models#all_models',                  :as=>"all_models",          :via =>:get
    match 'all_makers'         => 'makers#all_makers',                  :as=>"all_makers",          :via =>:get
    match 'all_station_types'  => 'station_types#all_station_types',    :as=>"all_station_types",   :via =>:get
    match 'all_fuel_locations' => 'fuel_locations#all_fuel_locations',  :as=>"all_fuel_locations",  :via =>:get
    match 'all_fuel_types'     => 'fuel_types#all_fuel_types',          :as=>"all_fuel_types",      :via =>:get
    
    ## dynamic pages
    resources :makers,          :except => [:index]
    resources :models,          :except => [:index]
    resources :envelopes,       :except => [:index]
    resources :fuel_types,      :except => [:show] 
    resources :fuel_locations,  :except => [:show]
    resources :station_types,   :except => [:show]
    
    ## users
    devise_for :users
      
    resources :users do
      member do
        put :save_roles
        get :edit_roles
        get :profile
      end
    end
    
    ## aircrafts
    resources :aircrafts do
      
      resources :aircraft_comments, :except =>[:index, :new]
      
      ## filter by maker and model
      collection do 
        get :filter_models
        get :filter_aircrafts
      end
      
      member do 
        get :clone
        get :weight_balance
        get :fly  
        get :report    
      end 
      
    end
    
    resources :aircraft_comments, :except => [:new]
    
    ## home page
    root :to => "static_pages#home"
  end
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
