module AircraftsHelper
  
  DEFAULT_IMAGE_NAME = "no-image.gif"
  
  ## checks wether the current aircraft has a specific image or not
  def has_default_image
    return @aircraft.image.url.include? DEFAULT_IMAGE_NAME
  end
  
  ## gets the maximum capacity of fuel in liters
  def max_fuel_liters
    max = 0
    
    @fuel_tanks.each do |tank|
      max += tank.volumeMax
    end
    
    return max
  end
  
  ## gets the maximum capacity of fuel in kilograms
  def max_fuel_kilos
    return max_fuel_liters * @aircraft.model.fuel_type.density
  end
  
  ## gets the maximum mass of all stations in kilograms
  def max_stations_kilos
    max = 0
    
    @stations.each do |station|
      max += station.weightMax
    end
    
    return max
  end
  
end
