module ApplicationHelper
  
  ## Removes a field dynamically using javascript
  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end
  
  ## Adds a field dynamically using javascript
  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end
  
  ## Builds current url with locale
  ## Needed for urls such as http://localhost:3000/en/envelopes/new?aircraft=9
  def build_locale_url(locale)
    return request.url.gsub("/#{I18n.locale}/", "/#{locale}/")
  end

end
