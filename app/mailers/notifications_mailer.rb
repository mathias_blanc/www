## Notification mailer for sending e-mails through a contact form

class NotificationsMailer < ActionMailer::Base

  default :from => "blancmathias@gmail.com"
  default :to => "blancmathias@gmail.com"

  def new_message(message)
    @message = message
    mail(:subject => "[weight-and-balance.net] #{message.subject}")
  end
  
  def report(email, report)
      
    @report = report
  
    mail :to => email, :from => 'blancmathias@gmail.com', :subject => 'Weight & Balance report'
  end

end
