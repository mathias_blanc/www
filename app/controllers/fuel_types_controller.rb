class FuelTypesController < ApplicationController
  
  ## user authentication
  before_filter :authenticate_user!, :except=>[:all_fuel_types]
  
    ### REST API specific methods ##################################################
  
  def all_fuel_types
    @fuel_types = FuelType.all
    
    respond_to do |format|
      format.json{ render :json => @fuel_types}
    end 
  end
  
  ### HTML + JSON REST ##################################################

  
  # GET /fuel_types
  # GET /fuel_types.json
  def index
    @fuel_types = FuelType.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @fuel_types }
    end
  end

  # GET /fuel_types/1
  # GET /fuel_types/1.json
  def show
    @fuel_type = FuelType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @fuel_type }
    end
  end

  # GET /fuel_types/new
  # GET /fuel_types/new.json
  def new
    @fuel_type = FuelType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @fuel_type }
    end
  end

  # GET /fuel_types/1/edit
  def edit
    @fuel_type = FuelType.find(params[:id])
  end

  # POST /fuel_types
  # POST /fuel_types.json
  def create
    @fuel_type = FuelType.new(params[:fuel_type])

    respond_to do |format|
      if @fuel_type.save
        format.html { redirect_to fuel_types_path, :notice => 'Fuel type was successfully created.' }
        format.json { render :json => @fuel_type, :status => :created, :location => @fuel_type }
      else
        format.html { render :action => "new" }
        format.json { render :json => @fuel_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /fuel_types/1
  # PUT /fuel_types/1.json
  def update
    @fuel_type = FuelType.find(params[:id])

    respond_to do |format|
      if @fuel_type.update_attributes(params[:fuel_type])
        format.html { redirect_to fuel_types_path, :notice => 'Fuel type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @fuel_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /fuel_types/1
  # DELETE /fuel_types/1.json
  def destroy
    @fuel_type = FuelType.find(params[:id])
    @fuel_type.destroy

    respond_to do |format|
      format.html { redirect_to fuel_types_path }
      format.json { head :no_content }
    end
  end
end
