class ModelsController < ApplicationController
  
  before_filter :authenticate_user!, :except => [:show, :all_models]
  
  ### REST API specific methods ##################################################
  def all_models
    @models = Model.all
    respond_to do |format|
      format.json {render :json=>@models}
    end
  end
  
  
  ### HTML + JSON REST ##################################################
  
  # GET /models
  # GET /models.json
  def index
    @models = Model.all
    @maker = Maker.find(params[:maker])
    
    if @maker
      @models = @maker.models.all
    end
      
    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @models }
    end
  end

  # GET /models/1
  # GET /models/1.json
  def show
    @model = Model.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @model }
    end
  end

  # GET /models/new
  # GET /models/new.json
  def new
    @model = Model.new
    @maker = Maker.find_by_id(params[:maker])
    @fuelTypes = FuelType.all
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @model }
    end
  end

  # GET /models/1/edit
  def edit
    @model = Model.find(params[:id])
    @maker = @model.maker
    @fuelTypes = FuelType.all
    
  end

  # POST /models
  # POST /models.json
  def create
    @model = Model.new(params[:model])
    @maker = @model.maker
    @fuelTypes = FuelType.all
    

    respond_to do |format|
      if @model.save
        format.html { redirect_to @model, :notice => t(:model_created, :name=>@model.name) }
        format.json { render :json => @model, :status => :created, :location => @model }
      else
        format.html { render :action => "new" }
        format.json { render :json => @model.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /models/1
  # PUT /models/1.json
  def update
    @model = Model.find(params[:id])

    respond_to do |format|
      if @model.update_attributes(params[:model])
        format.html { redirect_to @model, :notice => t(:model_updated, :name=>@model.name) }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @model.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /models/1
  # DELETE /models/1.json
  def destroy
    @model = Model.find(params[:id])
    @model.destroy

    respond_to do |format|
      format.html { redirect_to aircrafts_url }
      format.json { head :no_content }
    end
  end
end
