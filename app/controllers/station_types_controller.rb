class StationTypesController < ApplicationController
  
  ## user authentication
  before_filter :authenticate_user!, :except => [:all_station_types]
  
  ### REST API specific methods ##################################################
  
  def all_station_types
    @station_types = StationType.all
    respond_to do |format|
      format.json {render :json => @station_types}
    end
  end
  
  ### HTML + JSON REST ##################################################
  
  # GET /station_types
  # GET /station_types.json
  def index
    @station_types = StationType.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @station_types }
    end
  end

  # GET /station_types/1
  # GET /station_types/1.json
  def show
    @station_type = StationType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @station_type }
    end
  end

  # GET /station_types/new
  # GET /station_types/new.json
  def new
    @station_type = StationType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @station_type }
    end
  end

  # GET /station_types/1/edit
  def edit
    @station_type = StationType.find(params[:id])
  end

  # POST /station_types
  # POST /station_types.json
  def create
    @station_type = StationType.new(params[:station_type])

    respond_to do |format|
      if @station_type.save
        format.html { redirect_to station_types_path, :notice => 'Station type was successfully created.' }
        format.json { render :json => @station_type, :status => :created, :location => @station_type }
      else
        format.html { render :action => "new" }
        format.json { render :json => @station_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /station_types/1
  # PUT /station_types/1.json
  def update
    @station_type = StationType.find(params[:id])

    respond_to do |format|
      if @station_type.update_attributes(params[:station_type])
        format.html { redirect_to station_types_path, :notice => 'Station type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @station_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /station_types/1
  # DELETE /station_types/1.json
  def destroy
    @station_type = StationType.find(params[:id])
    @station_type.destroy

    respond_to do |format|
      format.html { redirect_to station_types_path }
      format.json { head :no_content }
    end
  end
end
