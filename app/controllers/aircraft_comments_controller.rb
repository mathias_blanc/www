class AircraftCommentsController < ApplicationController
  
  before_filter :authenticate_user!
  load_and_authorize_resource
  
  # GET /aircraft_comments
  # GET /aircraft_comments.json
  def index
    @aircraft_comments = AircraftComment.order("created_at DESC").page(params[:page]).per(15)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @aircraft_comments }
    end
  end

  # GET /aircraft_comments/1
  # GET /aircraft_comments/1.json
  def show
    @aircraft_comment = AircraftComment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @aircraft_comment }
    end
  end

  # GET /aircraft_comments/new
  # GET /aircraft_comments/new.json
  def new
    @aircraft_comment = AircraftComment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @aircraft_comment }
    end
  end

  # GET /aircraft_comments/1/edit
  def edit
    @aircraft_comment = AircraftComment.find(params[:id])
  end

  # POST /aircraft_comments
  # POST /aircraft_comments.json
  def create
    @aircraft = Aircraft.find(params[:aircraft_id])
    @aircraft_comment = @aircraft.aircraft_comments.build(params[:aircraft_comment])
    @aircraft_comment.user_id = current_user.id


    respond_to do |format|
      if @aircraft_comment.save
        format.html { redirect_to @aircraft, :notice => t(:comment_created) }
        format.json { render :json => @aircraft_comment, :status => :created, :location => @aircraft_comment }
      else
        format.html { render :action => "new" }
        format.json { render :json => @aircraft.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /aircraft_comments/1
  # PUT /aircraft_comments/1.json
  def update
    @aircraft_comment = AircraftComment.find(params[:id])

    respond_to do |format|
      if @aircraft_comment.update_attributes(params[:aircraft_comment])
        format.html { redirect_to @aircraft_comment.aircraft, :notice => t(:comment_updated) }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @aircraft_comment.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /aircraft_comments/1
  # DELETE /aircraft_comments/1.json
  def destroy
    @aircraft_comment = AircraftComment.find(params[:id])
    @aircraft_comment.destroy

    respond_to do |format|
      format.html { redirect_to @aircraft_comment.aircraft }
      format.json { head :no_content }
    end
  end
end
