## Manage static pages
class StaticPagesController < ApplicationController

  def home
    respond_to do |format|
      format.html {render :layout => false} ## use custom layout for home page
    end
  end

  def admin
    authorize! :admin, :show
    respond_to do |format|
      format.html 
    end
  end
    
  def mobile
    respond_to do |format|
      format.html 
    end
  end
  
  def faq
    respond_to do |format|
      format.html
    end
  end
end
