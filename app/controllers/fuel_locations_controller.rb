class FuelLocationsController < ApplicationController
  
  ## user authentication
  before_filter :authenticate_user!, :except => [:all_fuel_locations]
  
  ### REST API specific methods ##################################################
  
  def all_fuel_locations
    @fuel_locations = FuelLocation.all
    
    respond_to do |format|
      format.json{ render :json => @fuel_locations}
    end 
  end
  
  ### HTML + JSON REST ##################################################
  
  # GET /fuel_locations
  # GET /fuel_locations.json
  def index
    @fuel_locations = FuelLocation.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @fuel_locations }
    end
  end

  # GET /fuel_locations/1
  # GET /fuel_locations/1.json
  def show
    @fuel_location = FuelLocation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @fuel_location }
    end
  end

  # GET /fuel_locations/new
  # GET /fuel_locations/new.json
  def new
    @fuel_location = FuelLocation.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @fuel_location }
    end
  end

  # GET /fuel_locations/1/edit
  def edit
    @fuel_location = FuelLocation.find(params[:id])
  end

  # POST /fuel_locations
  # POST /fuel_locations.json
  def create
    @fuel_location = FuelLocation.new(params[:fuel_location])

    respond_to do |format|
      if @fuel_location.save
        format.html { redirect_to fuel_locations_path, :notice => 'Fuel location was successfully created.' }
        format.json { render :json => @fuel_location, :status => :created, :location => @fuel_location }
      else
        format.html { render :action => "new" }
        format.json { render :json => @fuel_location.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /fuel_locations/1
  # PUT /fuel_locations/1.json
  def update
    @fuel_location = FuelLocation.find(params[:id])

    respond_to do |format|
      if @fuel_location.update_attributes(params[:fuel_location])
        format.html { redirect_to fuel_locations_path, :notice => 'Fuel location was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @fuel_location.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /fuel_locations/1
  # DELETE /fuel_locations/1.json
  def destroy
    @fuel_location = FuelLocation.find(params[:id])
    @fuel_location.destroy

    respond_to do |format|
      format.html { redirect_to fuel_locations_path }
      format.json { head :no_content }
    end
  end
end
