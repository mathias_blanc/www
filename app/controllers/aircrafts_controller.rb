class AircraftsController < ApplicationController
  
  ## constants used to convert units when 
  ## doing weight and balance
  KG_TO_LBS         = 2.20462262
  M_TO_INCH         = 39.3700787
  L_TO_GAL          = 0.264172052
  INCH_LBS_TO_M_KG  = 0.0559974146
  KG_L_TO_LBS_GAL   = 8.34540445
  
  before_filter :authenticate_user!, :except => [:index, :show, :filter_models, :filter_aircrafts, :weight_balance, :fly, :all_aircrafts]    
  
  ### REST API specific methods ##################################################
  
  ## REST API
  ## GET /all_aircrafts
  def all_aircrafts
    
    @aircrafts = Aircraft.all
    respond_to do |format|
      format.json{ render :json =>@aircrafts.to_json(
              :include => { :stations => {}, 
                            :fuel_tanks => {}, 
                            :envelope => {:include => :coordinates}
                          }
            )
        }
    end
  end
  
  
  ### HTML + JSON REST ##################################################
  
  ## GET /aircraft/:id/weight_balance
  ## user input for weight and balance
  def weight_balance
    @aircraft = Aircraft.find(params[:id])
    @stations = @aircraft.stations.find(:all, :include => "station_type", :order => "station_types.position")
    @fuel_tanks = @aircraft.fuel_tanks.find(:all, :include =>"fuel_location", :order=> "fuel_locations.position")
    @comments = @aircraft.aircraft_comments.order("created_at DESC").page(params[:page]).per(4)
    @comments_count = @aircraft.aircraft_comments.count
    
    respond_to do |format|
      format.html
    end
  end
  
  ## sends report by email
  def report
    render :nothing => true, :status => 200
    NotificationsMailer.report(current_user.email, session["report"]).deliver
    
  end
  
      
  ## GET /aircraft/:id/fly
  ## Return results for weight and balance
  def fly
    @aircraft = Aircraft.find(params[:id])
    @stations = @aircraft.stations.find(:all, :include => "station_type", :order => "station_types.position")
    @fuel_tanks = @aircraft.fuel_tanks.find(:all, :include =>"fuel_location", :order=> "fuel_locations.position")
    @comments = @aircraft.aircraft_comments.order("created_at DESC").page(params[:page]).per(4)
    @comments_count = @aircraft.aircraft_comments.count
    @envelope = @aircraft.envelope
    
    max_allowed_fuel_liters = Array.new
    to_fuel = Array.new
    ld_fuel = Array.new
    
    ## converts string params to float
    @weights = params[:weights].collect{|i| i.to_f}
    to_fuels = params[:tofuel].collect{|i| i.to_f}
    ld_fuels = params[:ldfuel].collect{|i| i.to_f}
    
    @x_max = @envelope.coordinates.order("cg ASC").maximum(:cg)
    @x_min = @envelope.coordinates.order("cg ASC").minimum(:cg)
    @y_max = @envelope.coordinates.order("weight ASC").maximum(:weight)
    @y_min = @envelope.coordinates.order("weight ASC").minimum(:weight)
    
    ## converts all values to metric for calculations
    @units = params[:units]
      
    ## converts all aircraft values if using us units
    if @units && @units == "us"
      @aircraft.emptyWeight *= KG_TO_LBS
      @aircraft.emptyArm *= M_TO_INCH
      @aircraft.mtow *= KG_TO_LBS
      @aircraft.mldgw *= KG_TO_LBS 
      @aircraft.model.fuel_type.density *= KG_L_TO_LBS_GAL
      
      @x_max *= M_TO_INCH
      @x_min *= M_TO_INCH
      @y_max *= KG_TO_LBS
      @y_min *= KG_TO_LBS
      
      @stations.each do |station|
        station.arm *= M_TO_INCH
        station.weightMax *= KG_TO_LBS
      end
      
      @fuel_tanks.each do |tank|
        tank.arm *= M_TO_INCH
        tank.volumeMax *= L_TO_GAL
      end
      
      @envelope.coordinates.each do |coord|
        coord.cg *= M_TO_INCH
        coord.weight *= KG_TO_LBS
      end    
    end
  
    max_allowed_fuel = max_allowed_fuel_liters(@fuel_tanks)
    
    ## sum calculations
    @to_weight = aircraft_weight(@weights, to_fuels)
    @ld_weight = aircraft_weight(@weights, ld_fuels)
    @to_cg = aircraft_moment(@weights, to_fuels) / @to_weight
    @ld_cg = aircraft_moment(@weights, ld_fuels) / @ld_weight
    @to_fuel = to_fuels.inject(0){|sum,item| sum + item}
    @ld_fuel = ld_fuels.inject(0){|sum,item| sum + item}
    
    ## error handling
    @errors = Hash.new
    @errors["to_weight"] = t(:mtow_out, :value=>@aircraft.mtow) if @to_weight > @aircraft.mtow
    @errors["ld_weight"] = t(:mldgw_out, :value=>@aircraft.mldgw) if @ld_weight > @aircraft.mldgw
    @errors["to_cg"]     = t(:to_cg_out) unless point_in_envelope?(@to_cg, @to_weight, @envelope.coordinates.sort_by{|t| t.order})
    @errors["ld_cg"]     = t(:ld_cg_out) unless point_in_envelope?(@ld_cg, @ld_weight, @envelope.coordinates.sort_by{|t| t.order})
    @errors["to_fuel"]   = t(:to_fuel) if @to_fuel == 0 || @to_fuel > max_allowed_fuel
    @errors["ld_fuel"]   = t(:ld_fuel) if @ld_fuel > @to_fuel || @ld_fuel <= 0 || @ld_fuel > max_allowed_fuel
    
    report = Report.new
    report.to_weight = @to_weight
    report.ld_weight = @ld_weight
    report.to_fuel = @to_fuel
    report.ld_fuel = @ld_fuel
    report.to_cg = @to_cg
    report.ld_cg = @ld_cg
    report.errors = @errors
    
    session["report"] = report
    
    respond_to do |format|
      format.html
    end
  end
  
  ## Filters models by maker and renders a list through jquery
  def filter_models
    @maker = Maker.find(params[:maker])
    @models = @maker.models.order("name ASC")
    
    respond_to do |format|
      format.js
    end
  end
  
  ## Filters aircrafts by models and renders a list through jquery
  def filter_aircrafts
    @model = Model.find(params[:model])
    @aircrafts = @model.aircrafts.order("callSign ASC")
    
    respond_to do |format|
      format.js
    end
  end
  
  ## Clones an existing aircraft and its envelope
  def clone
    @old_craft = Aircraft.find(params[:id])
    @model = @old_craft.model
    @aircraft = @old_craft.duplicate
    
    ## required for nested stations and fuel tanks
    @stationTypes = StationType.find(:all, :order=>:position)
    @fuelLocations = FuelLocation.all
    
    respond_to do |format|
      format.html
    end
  end
  
  # GET /aircrafts
  # GET /aircrafts.json
  def index
    search = params[:search]
    
    if search ## search by specific call sign
      @aircrafts = Aircraft.find(:all, :conditions => ['callSign LIKE ?', "%#{search}%"])
      
      ## get all models by aircrafts
      @models = Array.new
      @aircrafts.each do |craft|
        @models<< craft.model  
      end
      
      ## get all makers by models
      @makers = Array.new
      @models.each do |model|
        @makers << model.maker
      end
      
      ## remove duplicates
      @models = @models.uniq
      @makers = @makers.uniq
      
    else ## display all aircrafts, models and makers
      @makers = Maker.order("name ASC")
      @models = Model.order("name ASC")
      @aircrafts = Aircraft.order("callSign ASC")
    end    

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @aircrafts }
    end
  end
   
  # GET /aircrafts/1
  # GET /aircrafts/1.json
  def show
    @aircraft = Aircraft.find(params[:id])
    @model = @aircraft.model
    @stations = @aircraft.stations.find(:all, :include => "station_type", :order => "station_types.position")
    @fuel_tanks = @aircraft.fuel_tanks.find(:all, :include =>"fuel_location", :order=> "fuel_locations.position")
    @comments = @aircraft.aircraft_comments.order("created_at DESC").page(params[:page]).per(4)
    @comments_count = @aircraft.aircraft_comments.count
    @updated_by = User.find(@aircraft.updated_by)
    @created_by = User.find(@aircraft.created_by)
    @envelope = @aircraft.envelope
    
    ## min and max values used to draw the envelope
    ## this has to be here because the drawing method is shared
    ## with weight and balance action which can be done using us and 
    ## metric units
    if @envelope
      @x_max = @envelope.coordinates.order("cg ASC").maximum(:cg)
      @x_min = @envelope.coordinates.order("cg ASC").minimum(:cg)
      @y_max = @envelope.coordinates.order("weight ASC").maximum(:weight)
      @y_min = @envelope.coordinates.order("weight ASC").minimum(:weight)
    end
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => [@aircraft, @stations, @fuel_tanks, @aircraft.envelope.coordinates] }
    end
  end

  # GET /aircrafts/new
  # GET /aircrafts/new.json
  def new
    @aircraft = Aircraft.new
    @model = Model.find(params[:model])
    
    ## required for nested stations and fuel tanks
    @stationTypes = StationType.find(:all, :order=>:position)
    @fuelLocations = FuelLocation.all
    
    ## one default station for pilot seat
    station = @aircraft.stations.build
    
    ## at least one fuel tank
    fuelTank = @aircraft.fuel_tanks.build
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @aircraft }
    end
  end

  # GET /aircrafts/1/edit
  def edit
    @aircraft = Aircraft.find(params[:id])
    @model = @aircraft.model
    
    ## required for nested stations and fuel tanks
    @stationTypes = StationType.all
    @fuelLocations = FuelLocation.all
  end

  # POST /aircrafts
  # POST /aircrafts.json
  def create
    @aircraft = Aircraft.new(params[:aircraft])
    @model = @aircraft.model
    
    ## required for nested stations and fuel tanks
    @stationTypes = StationType.all
    @fuelLocations = FuelLocation.all
    
    ## aicraft was cloned
    cloned_from = params[:cloned_from]
    
    ## normalize characteristics if using us units
    unit = params[:units]
    @aircraft.normalize = true if (unit && unit == "us")
    
    ## created by user
    @aircraft.created_by = current_user.id
    @aircraft.updated_by = current_user.id
    
    respond_to do |format|
      ## cloning envelope if duplicating the aircraft
      if cloned_from && Aircraft.find(cloned_from).envelope
        envelope = Envelope.new
        envelope = Aircraft.find(cloned_from).envelope.dup :include => [:coordinates]
        envelope.aircraft = @aircraft
        envelope.save
      end
      
      if @aircraft.save    
        format.html { redirect_to @aircraft, :notice => t(:aircraft_created, :name=>@aircraft.callSign) }
        format.json { render :json => @aircraft, :status => :created, :location => @aircraft }
      else
        format.html { render :action => "new" }
        format.json { render :json => @aircraft.errors, :status => :unprocessable_entity }
        
        #make sure that there at least a station and a fuel tank in case of error
        if @aircraft.stations.length < 1 
          station  = @aircraft.stations.build
        end
        
        if @aircraft.fuel_tanks.length < 1
          fuelTank = @aircraft.fuel_tanks.build
        end
        
      end
    end
  end

  # PUT /aircrafts/1
  # PUT /aircrafts/1.json
  def update
    @aircraft = Aircraft.find(params[:id])
    @model = @aircraft.model
    @stationTypes = StationType.all
    @fuelLocations = FuelLocation.all
    
    ## normalize aircraft characteristics if using us system
    unit = params[:units]
    @aircraft.normalize = true if (unit && unit == "us")
    
    ## updated by user
    @aircraft.updated_by = current_user.id

    respond_to do |format|
      if @aircraft.update_attributes(params[:aircraft])
        format.html { redirect_to @aircraft, :notice => t(:aircraft_updated, :name=>@aircraft.callSign) }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @aircraft.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /aircrafts/1
  # DELETE /aircrafts/1.json
  def destroy
    @aircraft = Aircraft.find(params[:id])
    @aircraft.destroy

    respond_to do |format|
      format.html { redirect_to aircrafts_url }
      format.json { head :no_content }
    end
  end
  
  ########################################################################
  ## private methods used for calculations
  private
  
  ## Returns the current aircraft total weight
  ## including weight stations and fuel based on density
  def aircraft_weight (weights, fuel_tanks)
    sum = @aircraft.emptyWeight
  
    weights.each do |weight|
      sum += weight
    end
    
    fuel_tanks.each do |fuel|
      sum += fuel * @aircraft.model.fuel_type.density
    end
    
    return sum
  end
  
  ## Returns the current aircraft moment
  def aircraft_moment(weights, fuel_tanks)
    sum = @aircraft.emptyArm * @aircraft.emptyWeight
    
    @stations.each_with_index do |station, n|
      sum += station.arm * weights[n]
    end
    
    @fuel_tanks.each_with_index do |tank, n|
      sum += tank.arm * fuel_tanks[n] * @aircraft.model.fuel_type.density
    end
    
    return sum
  end
  
  ## Return true if a point is located in inside the envelope
  ## ray casting http://local.wasp.uwa.edu.au/~pbourke/geometry/insidepoly/
  def point_in_envelope?(x, y, coords)
    c = false;
    
    for i in 0 .. coords.count - 1
      coord1 = coords[i]
      coord2 = coords[(i+1) % coords.count]
      
      if (y > [coord1.weight, coord2.weight].min) 
        if (y <= [coord1.weight, coord2.weight].max) 
          if (x <= [coord1.cg, coord2.cg].max) 
            if (coord1.weight != coord2.weight)
              xinterval = (y - coord1.weight) * (coord2.cg - coord1.cg) / (coord2.weight - coord1.weight) + coord1.cg
              if coord1.cg == coord2.cg || x <= xinterval
                c = !c
              end
            end
          end 
        end
      end
    end 
    
    return c  
  end
  
  ## Returns the total aircraft fuel capacity in liters
  def max_allowed_fuel_liters(fuel_tanks)
    max = 0
    
    fuel_tanks.each do |tank|
      max += tank.volumeMax
    end
    
    return max
  end
  
end
