class ApplicationController < ActionController::Base
  protect_from_forgery
  
  ## redirect after user sign in
  def after_sign_in_path_for(resource_or_scope)
    aircrafts_path
  end
  
  ## redirect after user sign out
  def after_sign_out_path_for(resource_or_scope)
    request.referrer
  end
  
  ## i18n
  before_filter :set_locale
 
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
  
  def default_url_options(options={})
    logger.debug "default_url_options is passed options: #{options.inspect}\n"
    { :locale => I18n.locale }
  end
end