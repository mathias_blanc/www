class BaseStationsController < ApplicationController
  
  ## user authentication
  before_filter :authenticate_user!
  
  # GET /base_stations
  # GET /base_stations.json
  def index
    @base_stations = BaseStation.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @base_stations }
    end
  end

  # GET /base_stations/1
  # GET /base_stations/1.json
  def show
    @base_station = BaseStation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @base_station }
    end
  end

  # GET /base_stations/new
  # GET /base_stations/new.json
  def new
    @base_station = BaseStation.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @base_station }
    end
  end

  # GET /base_stations/1/edit
  def edit
    @base_station = BaseStation.find(params[:id])
  end

  # POST /base_stations
  # POST /base_stations.json
  def create
    @base_station = BaseStation.new(params[:base_station])

    respond_to do |format|
      if @base_station.save
        format.html { redirect_to @base_station, :notice => 'Base station was successfully created.' }
        format.json { render :json => @base_station, :status => :created, :location => @base_station }
      else
        format.html { render :action => "new" }
        format.json { render :json => @base_station.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /base_stations/1
  # PUT /base_stations/1.json
  def update
    @base_station = BaseStation.find(params[:id])

    respond_to do |format|
      if @base_station.update_attributes(params[:base_station])
        format.html { redirect_to @base_station, :notice => 'Base station was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @base_station.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /base_stations/1
  # DELETE /base_stations/1.json
  def destroy
    @base_station = BaseStation.find(params[:id])
    @base_station.destroy

    respond_to do |format|
      format.html { redirect_to base_stations_url }
      format.json { head :no_content }
    end
  end
end
