class EnvelopesController < ApplicationController
  
  include UnitConversion
  
  ## user authentication
  before_filter :authenticate_user!
  
  # GET /envelopes
  # GET /envelopes.json
  def index
    @envelopes = Envelope.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @envelopes }
    end
  end

  # GET /envelopes/1
  # GET /envelopes/1.json
  def show
    @envelope = Envelope.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @envelope }
    end
  end

  # GET /envelopes/new
  # GET /envelopes/new.json
  def new
    @envelope = Envelope.new
    @aircraft = Aircraft.find(params[:aircraft])
    @coord_order = 0 #keeps track of dynamic fields order when creating the envelope
    
    ## one default station for pilot seat
    4.times do
      coordinate = @envelope.coordinates.build
    end
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @envelope }
    end
  end
  

  # GET /envelopes/1/edit
  def edit
    @envelope = Envelope.find(params[:id])
    @aircraft = @envelope.aircraft
    @coord_order = 0 #keeps track of dynamic fields order when creating the envelope
  end

  # POST /envelopes
  # POST /envelopes.json
  def create
    @envelope = Envelope.new(params[:envelope])  
    @aircraft = @envelope.aircraft
    @coord_order = 0 #keeps track of dynamic fields order when creating the envelope
    
    ## normalize envelope coordinates if using us units
    unit = params[:units]
    @envelope.normalize = true if (unit && unit == "us")
    
    ## aircraft was updated by user
    @aircraft.update_attributes(:updated_by => current_user.id)
    
    respond_to do |format|
      if @envelope.save
        format.html { redirect_to @envelope.aircraft, :notice => t(:envelope_created) }
        format.json { render :json => @envelope, :status => :created, :location => @envelope }
      else
        format.html { render :action => "new" }
        format.json { render :json => @envelope.errors, :status => :unprocessable_entity }
        
        #make sure that all 4 field are available
        (4 - @envelope.coordinates.length).times do
          coordinate = @envelope.coordinates.build
        end
      end
    end
  end

  # PUT /envelopes/1
  # PUT /envelopes/1.json
  def update
    @envelope = Envelope.find(params[:id])
    @aircraft = @envelope.aircraft
    @coord_order = 0 #keeps track of dynamic fields order when creating the envelope
    
    ## normalize envelope coordinates if using us units
    unit = params[:units]
    @envelope.normalize = true if (unit && unit == "us")
    
    ## aircraft was updated by user
    @aircraft.update_attributes(:updated_by => current_user.id)
    
    respond_to do |format|
      if @envelope.update_attributes(params[:envelope])
        format.html { redirect_to @envelope.aircraft, :notice => t(:envelope_updated) }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @envelope.errors, :status => :unprocessable_entity }
        
        #make sure that all 4 field are available
        (4 - @envelope.coordinates.length).times do
          coordinate = @envelope.coordinates.build
        end
      end
    end
  end

  # DELETE /envelopes/1
  # DELETE /envelopes/1.json
  def destroy
    @envelope = Envelope.find(params[:id])
    @envelope.destroy

    respond_to do |format|
      format.html { redirect_to envelopes_url }
      format.json { head :no_content }
    end
  end
end
