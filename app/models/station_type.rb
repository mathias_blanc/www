class StationType < ActiveRecord::Base

  self.include_root_in_json = true
  
  ## default sort order
  default_scope :order => "position ASC"
  
  ## associations
  has_one :station, :class_name => BaseStation
  
  ## validations
  validates_presence_of :en
  validates_presence_of :fr
  validates_presence_of :position

end
