class BaseStation < ActiveRecord::Base

  self.include_root_in_json = true
  
  #associations
  belongs_to :aircraft
  
end
