class AircraftComment < ActiveRecord::Base
  
  self.include_root_in_json = true
  
  ## associations
  belongs_to :aircraft
  belongs_to :user

end
