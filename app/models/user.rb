class User < ActiveRecord::Base

  self.include_root_in_json = true
  
  ## default role for users
  before_create :setup_role

  ## associations
  has_and_belongs_to_many :roles
  has_many :aircraft_comments, :dependent => :destroy
  
  ## validations
  validates_presence_of :username
  validates_uniqueness_of :username
  
  ## image
  has_attached_file :avatar, 
                    :url => '/system/:class/:attachment/:id/:style/:filename',
                    :styles => {:tiny   => ["75x75#",   :png],
                                :thumb  => ["90x90#", :png], 
					                      :small  => ["100x100>", :png]},
                    :default_url => 'no-avatar/:style/no-image.png'

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  ## Virtual attribute for authenticating by either username or email
  attr_accessor :login
  
  ## Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :username, :role_ids, :login, :avatar
  
  ## checks whether a user has a specific role
  def has_role?(role)
    return !!self.roles.find_by_name(role.to_s.camelize)
  end
  
  ## set default role for new users
  def setup_role
    self.role_ids = [1]
  end
  
  ## overwrite authentication model so that a user can sign in using either his username or e-mail address
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end
  
end
