class Model < ActiveRecord::Base

  self.include_root_in_json = true
  
  #associations
  belongs_to :maker
  belongs_to :fuel_type
  has_many   :aircrafts, :dependent => :destroy
  
  #validations
  validates_presence_of   :name
  validates_uniqueness_of :name
  validates_associated    :maker
  
  #image
  has_attached_file :image, 
                    :url => '/system/:class/:attachment/:id/:style/:filename',
                    :styles => {:tiny   => ["75x75#",   :png],
                                :thumb  => ["100x100#", :png], 
					                      :small  => ["150x150>", :png],  
                					      :medium => ["300x300>", :png], 
                                :large  => ["500x500>", :png]},
                    :default_url => 'no-image/:style/no-image.gif'

end
