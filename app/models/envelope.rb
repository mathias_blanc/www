class Envelope < ActiveRecord::Base
  
  self.include_root_in_json = true
  
  LBS_TO_KG = 0.45359237
  INCH_TO_M = 0.0254
  
  ## determines whether the envelope has to be normalized or not (converted to metric)
  attr_accessor :normalize
  before_save :normalize_envelope
  validate :validate_coordinates_count
  
  ## associations
  belongs_to  :aircraft
  has_many    :coordinates, :dependent => :destroy, :order => "coordinates.order ASC" ## sorts envelope coordinates by order
  
  ## nested attriutes
  accepts_nested_attributes_for :coordinates, :reject_if => lambda { |a| a[:weight].blank? || a[:cg].blank? }, :allow_destroy => true
  
  ## check that there are at list 4 limit points
  def validate_coordinates_count
    errors.add(:base, 'At least four limit points!') if self.coordinates.length < 4
    return true
  end
  
  ## normalize envelope if input was in us system
  def normalize_envelope
    if self.normalize
      self.coordinates.each do |coord|
        coord.cg *= INCH_TO_M
        coord.weight *= LBS_TO_KG
      end
      
      self.normalize = false
      
      save!
    end
  end
  
end