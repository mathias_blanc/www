class Ability
  include CanCan::Ability

  def initialize(user)
  
    user ||= User.new # guest user (not logged in)
    
    if user.has_role?(:admin)
      can :manage, :all
      can :show, :admin
    end
    
    if user.has_role?(:user)
      can :manage, Aircraft
      can :manage, Model
      can :manage, Maker
      
      ## a user can only manage his own account
      can :manage, User, :id => user.id
      can :manage, AircraftComment
    else
      can :read, Aircraft
      can :read, Model
      can :read, Maker 
    end
    
    can :home
    can :contact
    can :mobile 

  end
end


