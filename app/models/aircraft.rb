class Aircraft < ActiveRecord::Base
    
  LBS_TO_KG = 0.45359237
  INCH_TO_M = 0.0254
  GAL_TO_L = 3.78541178
  
  ## determines whether the aircraft has to be normalized or not (converted to metric)
  attr_accessor :normalize
  
  ## associations
  belongs_to  :model
  has_one     :envelope,          :dependent => :destroy
  has_many    :stations,          :dependent => :destroy
  has_many    :fuel_tanks,        :dependent => :destroy
  has_many    :aircraft_comments, :dependent => :destroy
  
  ## validations
  validates_presence_of :callSign, :emptyWeight, :emptyArm, :mtow, :mldgw
  validates_uniqueness_of :callSign
  validates_associated :model
  validate :validate_stations_count, :validate_fuel_tanks_count
  before_save :normalize_aircraft
  
  
  ## image
  has_attached_file :image, 
                    :url => '/system/:class/:attachment/:id/:style/:filename',
                    :styles => {:tiny   => ["75x75#",   :png],
                                :thumb  => ["100x100#", :png], 
					                      :small  => ["150x150>", :png],  
                					      :medium => ["300x300>", :png], 
                                :large  => ["500x500>", :png]},
                    :default_url => 'no-image/:style/no-image.gif'
  
  ## nested attributes -> stations + fuel tanks
  accepts_nested_attributes_for :stations, :reject_if => lambda { |a| a[:arm].blank? }, :allow_destroy => true
  accepts_nested_attributes_for :fuel_tanks, :reject_if => lambda { |a| a[:arm].blank? }, :allow_destroy => true
  
  ## duplicates current aircraft
  def duplicate
    craft = Aircraft.new
    craft = self.dup :include => [:stations, :fuel_tanks, :envelope]    
    craft.callSign = ""
        
    return craft
  end
  
  ## validates the number of stations
  def validate_stations_count
    if self.stations.length < 1
      errors.add(:base, 'At least one station for pilot seat!')
    end
    
    ## check for stations marked for destruction
    to_be_deleted = 0
    
    self.stations.each do |station|
      if station.marked_for_destruction?
        to_be_deleted += 1
      end
    end
    
    if to_be_deleted == self.stations.length
      errors.add(:base, 'At least one station for pilot seat')
    end
    
    return true
  end
  
  ## validates the number of fuel tanks
  def validate_fuel_tanks_count
    if self.fuel_tanks.length < 1
      errors.add(:base, 'At least one fuel tank!')
    end
    
    ## check for fuel tanks marked for destruction
    to_be_deleted = 0
    
    self.fuel_tanks.each do |tank|
      if tank.marked_for_destruction?
        to_be_deleted += 1
      end
    end
    
    if to_be_deleted == self.fuel_tanks.length
      errors.add(:base, 'At least one fuel tank!')
    end
    
    return true
  end
  
  ## converts all units to metric
  def normalize_aircraft
    if self.normalize
      
      self.emptyWeight  *= LBS_TO_KG
      self.emptyArm     *= INCH_TO_M
      self.mtow         *= LBS_TO_KG
      self.mldgw        *= LBS_TO_KG
      
      self.stations.each do |station|
        station.arm       *= INCH_TO_M
        station.weightMax *= LBS_TO_KG
      end
      
      self.fuel_tanks.each do |tank|
        tank.arm        *= INCH_TO_M
        tank.volumeMax  *= GAL_TO_L
      end
      
      self.normalize = false
      save!
    end
  end
  
end
