$(function(){
  
  var searchText = "call sign";
  
  $('#s').focus(function(){
    if($(this).val() == searchText){
      $(this).val("");
      $(this).css("color", "#000");
    }
  });
  
  $('#s').blur(function(){
    if($(this).val() == ""){
      $(this).val(searchText);
      $(this).css("color", "#888");
    }
  });

})