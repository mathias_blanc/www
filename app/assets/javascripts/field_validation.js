$(function(){

  var floatRegExp = new RegExp("-?^[0-9]*[.]?[0-9]+$");
  
  /*
    If the value does not match the float regexp, we check
    for the first occurrence of a dot sign and then remove
    all characters different than numbers. If a dot sign was
    present we then put it back at its original place. This
    ensures that there is only one decimal sign in the value.
  */  
  function ensureFloatValue(element) {
    $(element).keyup(function(event) {
      var val = $(this).val();
      
      if(!floatRegExp.test(val)){
        var dotPos = val.indexOf(".");
        val = val.replace(/[^\d]/g, "");
        
        if(dotPos != -1)
          val = val.substr(0,dotPos) + "." + val.substr(dotPos);
      
        $(this).val(val);
      }
    
    });
  }
  
  //check all numeric fields
  ensureFloatValue(".massInput");
  ensureFloatValue(".lengthInput");
  ensureFloatValue(".volumeInput");
  ensureFloatValue(".massInputSmall");
  ensureFloatValue(".lengthInputSmall");
  ensureFloatValue(".volumeInputSmall");
  ensureFloatValue(".massInputSmaller");
  ensureFloatValue(".lengthInputSmaller");
  ensureFloatValue(".volumeInputSmaller");
  ensureFloatValue(".massInput150");
  ensureFloatValue(".lengthInput150");

})