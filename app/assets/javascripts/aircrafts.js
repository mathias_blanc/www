// removes a dynamic field from the form
function remove_fields(link) {
  $(link).prev("input[type=hidden]").val("1");
  $(link).closest(".fields").fadeOut(150);
}

// adds a dynamic field to the form
function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g");
  
  $(link).parent().before(content.replace(regexp, new_id));
  
  //IMPORTANT! this is used to put an ordering index when adding coordinates
  //to the envelope because the default creation order is not kept
  $('.order').each(function(index){
    $(this).val(index);
  });
}








