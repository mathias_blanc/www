const LBS_TO_KG         = 0.45359237;
const INCH_TO_M         = 0.0254;
const GAL_TO_L          = 3.78541178;
const M_KG_TO_INCH_LBS  = 17.8579673;

const KG_TO_LBS         = 2.20462262;
const M_TO_INCH         = 39.3700787;
const L_TO_GAL          = 0.264172052;
const INCH_LBS_TO_M_KG  = 0.0559974146;

const PRECISION = 2;

$(function(){


  $(document).ready(function() {
    if($('#unit_field').val() == "us"){
      $('#us_unit').attr('class', 'active');
      $('#si_unit').removeAttr('class');
    }
  });
  
  /********************************************/  
  $('#si_unit').click(function(){  
    if($(this).hasClass('active'))
      return;
      
    /* changes the unit button to current unit*/
    $('#si_unit').attr('class', 'active');
    $('#us_unit').removeAttr('class');
    
    updateUnitText("[kg]", " [m]", "[l]", "[m/kg]");
    updateStaticValues(LBS_TO_KG, INCH_TO_M, GAL_TO_L, INCH_LBS_TO_M_KG);
    updateInputValues(LBS_TO_KG, INCH_TO_M, GAL_TO_L, "si");
  });
  
  /********************************************/
  $('#us_unit').click(function(){
    if($(this).hasClass('active'))
      return;
    
    /* changes the unit button to current unit*/
    $('#us_unit').attr('class', 'active');
    $('#si_unit').removeAttr('class');
    
    updateUnitText("[lbs]", "[in]", "[gal]", "[in/lbs]");
    updateStaticValues(KG_TO_LBS, M_TO_INCH, L_TO_GAL, M_KG_TO_INCH_LBS);
    updateInputValues(KG_TO_LBS, M_TO_INCH, L_TO_GAL, "us");
  });
})

// Units to text 
function updateUnitText(mass, length, volume, cg){
  $('.mass-unit').each(function(){
    $(this).html(mass);
  });
  
  $('.length-unit').each(function(){
    $(this).html(length);
  });
  
  $('.volume-unit').each(function(){
    $(this).html(volume);
  }); 
  
  $('.cg-unit').each(function(){
    $(this).html(cg);
  }); 
}

// Static values
function updateStaticValues(mass, length, volume, cg){
  $('.static-mass').each(function(){
    var val = $(this).text();
    val *= mass;
    $(this).html(val.toFixed(PRECISION));
  }); 
  
  $('.static-length').each(function(){
    var val = $(this).text();
    val *= length;
    $(this).html(val.toFixed(PRECISION));
  }); 
  
  $('.static-volume').each(function(){
    var val = $(this).text();
    val *= volume;
    $(this).html(val.toFixed(PRECISION));
  });  
  
  $('.static-cg').each(function(){
    var val = $(this).text();
    val *= cg;
    $(this).html(val.toFixed(PRECISION));
  });
}

// Input values
function updateInputValues(mass, length, volume, units){
   
  convert('.volumeInputSmall', volume);
  convert('.massInputSmall', mass);
  convert('.lengthInputSmall', length);
  convert('.volumeInput', volume);
  convert('.massInput', mass);
  convert('.lengthInput', length);
  convert('.massInput150', mass);
  convert('.lengthInput150', length);
  
  //hidden parameter
  $('#unit_field').val(units);
}

/********************************************/
//Converts input fields
function convert(field, factor){
  $(field).each(function(){
    var val = $(this).val();
    
    if(isNaN(val))
      val = 0;
    
    val *= factor;
    
    $(this).val(val.toFixed(PRECISION));
  });
}