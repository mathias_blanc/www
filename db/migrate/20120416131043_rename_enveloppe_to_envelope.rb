class RenameEnveloppeToEnvelope < ActiveRecord::Migration
  def change
    rename_table :enveloppes, :envelopes
  end
end
