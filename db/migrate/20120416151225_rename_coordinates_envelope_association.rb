class RenameCoordinatesEnvelopeAssociation < ActiveRecord::Migration
  def up
    rename_column :coordinates, :enveloppe_id, :envelope_id
  end

  def down
    rename_column :coordinates, :envelope_id, :enveloppe_id
  end
end
