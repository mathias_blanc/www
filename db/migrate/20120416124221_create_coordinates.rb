class CreateCoordinates < ActiveRecord::Migration
  def change
    create_table :coordinates do |t|
      t.float :mass
      t.float :cg
      t.integer :enveloppe_id

      t.timestamps
    end
  end
end
