class CoordinatesHaveAnOrder < ActiveRecord::Migration
  def up
    add_column :coordinates, :order, :integer
  end

  def down
    remove_column :coordinates, :order
  end
end
