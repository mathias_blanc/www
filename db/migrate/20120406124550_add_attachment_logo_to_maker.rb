class AddAttachmentLogoToMaker < ActiveRecord::Migration
  def self.up
    add_column :makers, :logo_file_name, :string
    add_column :makers, :logo_content_type, :string
    add_column :makers, :logo_file_size, :integer
    add_column :makers, :logo_updated_at, :datetime
  end

  def self.down
    remove_column :makers, :logo_file_name
    remove_column :makers, :logo_content_type
    remove_column :makers, :logo_file_size
    remove_column :makers, :logo_updated_at
  end
end
