class RenameMassToWeightAndMomentToArm < ActiveRecord::Migration
  def up
    rename_column :base_stations, :moment, :arm
    rename_column :base_stations, :massMax, :weightMax
    rename_column :coordinates, :mass, :weight
  end

  def down
    rename_column :base_stations, :arm, :moment 
    rename_column :base_stations, :weightMax, :massMax
    rename_column :coordinates, :weight, :mass
  end
end
