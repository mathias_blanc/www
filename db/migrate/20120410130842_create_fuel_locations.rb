class CreateFuelLocations < ActiveRecord::Migration
  def change
    create_table :fuel_locations do |t|
      t.string :name

      t.timestamps
    end
  end
end
