class CreateBaseStations < ActiveRecord::Migration
  def change
    create_table :base_stations do |t|
      t.float :moment
      t.string :type
      t.integer :aircraft_id

      t.timestamps
    end
  end
end
