class AddPositionAndi18nToStationTypes < ActiveRecord::Migration
  def up
    add_column :station_types, :position, :integer
    add_column :station_types, :fr, :string
    rename_column :station_types, :name, :en
  end
  
  def down
    remove_column :station_types, :position, :integer
    remove_column :station_types, :fr, :string
    rename_column :station_types, :en, :name
  end
end
