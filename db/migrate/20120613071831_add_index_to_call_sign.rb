class AddIndexToCallSign < ActiveRecord::Migration
  def up
    add_index :aircrafts, :callSign
  end
  
  def down
    remove_index :aircrafts, :callSign
  end
end
