class ModelHasOneFuelType < ActiveRecord::Migration
  def up
    add_column :models, :fuel_type_id, :integer
  end

  def down
    remove_column :models, :fuel_type_id
  end
end
