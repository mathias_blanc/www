class EnvelopesHaveAircraftId < ActiveRecord::Migration
  def up
    remove_column :aircrafts, :envelope_id
    add_column :envelopes, :aircraft_id, :integer
  end

  def down
    add_colum :aircrafts, :envelope_id, :integer
    remove_column :envelopes, :aircraft_id
  end
end
