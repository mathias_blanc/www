class BaseStationHasNoMassAndVolume < ActiveRecord::Migration
  def up
    remove_column :base_stations, :mass
    remove_column :base_stations, :volume
  end

  def down
    add_column :base_stations, :mass, :float
    add_column :base_stations, :volume, :float
  end
end
