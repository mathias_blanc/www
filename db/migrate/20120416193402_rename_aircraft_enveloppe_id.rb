class RenameAircraftEnveloppeId < ActiveRecord::Migration
  def up
    rename_column :aircrafts, :enveloppe_id, :envelope_id
  end

  def down
    rename_column :aircrafts, :envelope_id, :enveloppe_id
  end
end
