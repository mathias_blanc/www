class RenameMassAndMomentToWeightAndArm < ActiveRecord::Migration
  def up
    rename_column :aircrafts, :emptyMass, :emptyWeight
    rename_column :aircrafts, :emptyMoment, :emptyArm
  end

  def down
    rename_column :aircrafts, :emptyWeight, :emptyMass
    rename_column :aircrafts, :emptyArm, :emptyMoment
  end
end
