class CreateAircrafts < ActiveRecord::Migration
  def change
    create_table :aircrafts do |t|
      t.string :callSign
      t.float :emptyMass
      t.float :emptyMoment
      t.float :mtow
      t.float :mldgw
      t.integer :model_id

      t.timestamps
    end
  end
end
