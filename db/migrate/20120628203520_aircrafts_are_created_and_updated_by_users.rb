class AircraftsAreCreatedAndUpdatedByUsers < ActiveRecord::Migration
  def up
    add_column :aircrafts, :created_by, :integer
    add_column :aircrafts, :updated_by, :integer
  end

  def down
    remove_column :aircrafts, :created_by
    remove_column :aircrafts, :updated_by
  end
end
