class CreateStationTypes < ActiveRecord::Migration
  def change
    create_table :station_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
