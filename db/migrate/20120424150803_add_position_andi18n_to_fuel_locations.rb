class AddPositionAndi18nToFuelLocations < ActiveRecord::Migration
  def up
    add_column :fuel_locations, :position, :integer
    add_column :fuel_locations, :fr, :string
    rename_column :fuel_locations, :name, :en
  end
  
  def down
    remove_column :fuel_locations, :position, :integer
    remove_column :fuel_locations, :fr, :string
    rename_column :fuel_locations, :en, :name
  end
end
