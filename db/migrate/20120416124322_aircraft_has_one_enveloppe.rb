class AircraftHasOneEnveloppe < ActiveRecord::Migration
  def up
    add_column :aircrafts, :enveloppe_id, :integer
  end

  def down
    remove_column :aircrafts, :enveloppe_id
  end
end
