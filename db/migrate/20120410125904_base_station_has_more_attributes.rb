class BaseStationHasMoreAttributes < ActiveRecord::Migration
  def up
    add_column :base_stations, :mass, :float
    add_column :base_stations, :massMax, :float
    add_column :base_stations, :volume, :float
    add_column :base_stations, :volumeMax, :float
    add_column :base_stations, :station_type_id, :integer
    add_column :base_stations, :fuel_location_id, :integer
  end

  def down
    remove_column :base_stations, :mass
    remove_column :base_stations, :massMax
    remove_column :base_stations, :volume
    remove_column :base_stations, :volumeMax
    remove_column :base_stations, :station_type_id
    remove_column :base_stations, :fuel_location_id
  end
end
